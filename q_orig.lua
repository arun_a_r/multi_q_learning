--This program is to mimic the Qlearning in ARGoS by Amar

-- initialization
function init()
robot.colored_blob_omnidirectional_camera.enable()
--this to enable the camera on robot at initial time
end

-- actions for each step
function step()
    -- each step, log "Hello world."
    log("Running")
    sensingLeft =     robot.proximity[3].value +
                      robot.proximity[4].value +
                      robot.proximity[5].value +
                      robot.proximity[6].value +
                      robot.proximity[2].value

    sensingFront =    robot.proximity[1].value +
                      robot.proximity[24].value

    sensingRight =    robot.proximity[19].value +
                      robot.proximity[20].value +
                      robot.proximity[21].value +
                      robot.proximity[22].value +
                      robot.proximity[23].value
    if #robot.colored_blob_omnidirectional_camera > 0 then
      robot.wheels.set_velocity(10,-10)
        log("Found colored ball")
    end
    if sensingLeft ~= 0 and #robot.colored_blob_omnidirectional_camera == 0 then
      robot.wheels.set_velocity(7,5)
      --log(sensingLeft)
    elseif sensingRight ~= 0 and #robot.colored_blob_omnidirectional_camera == 0 then
      robot.wheels.set_velocity(5,7)
      --log(sensingRight)
    elseif sensingFront ~= 0 and #robot.colored_blob_omnidirectional_camera == 0 then
      a = robot.random.bernoulli(0.5)
      log(a)
      if a == 0 then
        robot.wheels.set_velocity(90,1)
      elseif a == 1 then
        robot.wheels.set_velocity(1,90)
      end
    elseif #robot.colored_blob_omnidirectional_camera == 0 then
      robot.wheels.set_velocity(10,10)
    end
end





-- reset the controller
function reset()
end

function destroy()
end
